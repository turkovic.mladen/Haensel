#! python2
import matplotlib.pyplot as plt
import os
output_file_path = os.path.join(os.path.dirname(__file__),
                                "..\\..\\..\\output\\")


def me(data, data_name):

    plot_file = os.path.join(output_file_path, (data_name + "_plot.pdf"))

    print("plotting " + data_name + "....")
    plt.hist(data, bins=500)

    plt.savefig(plot_file)

    plt.clf()
