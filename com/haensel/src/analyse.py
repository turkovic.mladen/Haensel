#! python2
# Outputs results of analysis to the Haensel/output folder
import numpy as np
import pandas as pd
import os

pd.set_option("display.max_rows", None)
pd.set_option('display.max_columns', None)

output_file_path = os.path.join(os.path.dirname(__file__),
                                "..\\..\\..\\output\\")


def pearson(data):

    pearson_file = os.path.join(output_file_path, "analyse_pearson.txt")
    text_file = open(pearson_file, "w")

    print(" >>  Pearson correlation coefficients: ")
    correlation_data = data.corr()

    # turn into absolute values
    correlation_data = correlation_data.abs()
    # replace diagonal correlation of 1 with 0
    np.fill_diagonal(correlation_data.values, 0)

    # calculate Maximum position and values of correlation coefficients
    idx_max_data = \
        pd.concat([correlation_data.idxmax(), correlation_data.max()],
                  axis=1, names=['Col_Nr.', 'Val.'])

    text_file.write(">>  Maximum position and values of correlation "
                    "coefficients:\n")
    text_file.write(str(idx_max_data))

    text_file.close()


def print2file(file_out, content):
    file_out.write("{0}".format(str(content)))


def me(data):

    me_file = os.path.join(output_file_path, "analyse_me.txt")
    text_file = open(me_file, "w")

    # make data-frame report
    report_df = data.dtypes.to_frame(name='dtypes')

    # Get min and maximum values for columns
    data_max = data.max()
    data_min = data.min()

    # count max elements > 1
    print("data_max > 1\n" + str(sum(sum([data_max > 1])[:])))

    # count min elements < 0
    print("data_min < 0\n" + str(sum(sum([data_min < 0])[:])))

    # add to report data-frame
    report_df['min'] = data_min.to_frame()
    report_df['max'] = data_max.to_frame()
    # print(report_df)

    # print to file
    print2file(text_file, report_df)
    print2file(text_file, "\nMIN/MAX\t\t")
    print2file(text_file, data_min.min())
    print2file(text_file, "\t")
    print2file(text_file, data_max.max())

    text_file.close()


def overview(data):

    overview_file = os.path.join(output_file_path, "analyse_overview.txt")
    text_file = open(overview_file, "w")

    print(" > Type overview>")
    print(data.columns.to_series().groupby(data.dtypes).groups)

    try:
        print2file(text_file, "\n\n > Float:\n")
        print2file(text_file, data.describe(include=['float64']))
    except ValueError:
        print2file(text_file, " N/A\n")

    try:
        print2file(text_file, "\n\n > Integers:\n")
        print2file(text_file, data.describe(include=['int64']))
    except ValueError:
        print2file(text_file, " N/A\n")

    try:
        print2file(text_file, "\n\n > Objects/ strings:\n")
        print2file(text_file, data.describe(include=['O']))
    except ValueError:
        print2file(text_file, " N/A\n")
