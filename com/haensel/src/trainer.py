#! python2
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import mean_squared_error, mean_absolute_error, \
    explained_variance_score, r2_score
import cPickle
import numpy as np


def classification(clf,
                   data_train_X, data_train_y,
                   data_test_X, data_test_y,
                   model_name):

    clf.fit(data_train_X, data_train_y)

    # save the classifier
    print "Saving classification model as ../models/" + model_name + \
          ".pkl ..."
    with open("../models/" + model_name + '.pkl', 'wb') as fid:
        cPickle.dump(clf, fid)

    print " Accuracy score: ", clf.score(data_test_X, data_test_y), "\n"

    y_predicted = clf.predict(data_test_X)

    # get the list of labels
    target_names = np.unique(data_test_y)[:].tolist()

    print "Confusion matrix\n   ", "\t".join(target_names), "\n", \
        confusion_matrix(data_test_y, y_predicted, labels=target_names)

    print "Classification report:"
    print classification_report(data_test_y, y_predicted,
                                target_names=target_names)


def regression(reg,
               data_train_X, data_train_y,
               data_test_X, data_test_y,
               model_name):

    reg.fit(data_train_X, data_train_y)

    # save the classifier
    print "Saving regression model as ../models/" + model_name + '.pkl', \
        " ..."
    with open("../models/" + model_name + '.pkl', 'wb') as fid:
        cPickle.dump(reg, fid)

    y_pred = reg.predict(data_test_X)

    print " Score: ", reg.score(data_test_X, data_test_y)
    print " mean_squared_error: ", mean_squared_error(data_test_y, y_pred)
    print " mean_absolute_error: ", mean_absolute_error(data_test_y, y_pred)
    print " explained_variance_score: ", explained_variance_score(
        data_test_y, y_pred)
    print " R^2 score: ", r2_score(data_test_y, y_pred), "\n"


def predict(model_name, data_test_X):

    print
    print "Making predictions for", model_name, "model. Please wait..."

    with open("model/" + model_name + ".pkl", 'rb') as fid:
        model_loaded = cPickle.load(fid)

    return model_loaded.predict(data_test_X)
