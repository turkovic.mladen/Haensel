#! python2
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.neural_network import MLPClassifier

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

import loader as ld
import analyse as analyse
import trainer as train
import fix_me as fx
import plot as plot
import os


output_file_path = os.path.join(os.path.dirname(__file__), "..\\data\\")
file_name = os.path.join(output_file_path, "sample.csv")

# Load the data
data = ld.get_csv(file_name, False)

# ## 1. Make some initial data analysis.                                ## #

# Try to find some structures in the data and
#  how attributes are possibly connected.

# print analysis of data, remove class
analyse.me(data.iloc[:, :-1])
analyse.overview(data.iloc[:, :-1])

# SLOW
analyse.pearson(data.iloc[:, :-1])

# Analyse the class frequency
print "Class Distribution:\n", data.iloc[:, -1].value_counts()

# Plot some data
plot.me(data.iloc[:, 1], "column_1")
plot.me(data.iloc[:, 2], "column_2")
plot.me(data.iloc[:, 3], "column_3")

# ## 1.5 Pre-processing some input data                                 ## #
# The analysis indicated that columns 23 and 36 should be preprocessed

# Column 23 is seemingly using -1 instead of 0 so this will be 'fixed'
# Column 36 has only 7 values < 0, possible errors in reading ?
#           due to the relatively low count these will be replaced by 0
data = fx.negative_2_zero(data, 23)
data = fx.negative_2_zero(data, 36)

# Columns 3, 36, 43, 64, 294 contain data with very high standard deviation
# using log transform to fix that
for c in [3, 36, 43, 64, 294]:
    data = fx.log_transform(data, c)

# remove columns 59, 179 and from 268 to 276 since have standard deviation 0
#     i.e. only one value
data = data.drop(data.columns[268:276], axis=1)
data = data.drop(data.columns[179], axis=1)
data = data.drop(data.columns[59], axis=1)

# ## 2. Fit some ML model(s).                                             ## #
# Under the models should definitely be some Neural Network and
#  please explain briefly your choices.

# convert to ndarray data
nd_data = data.as_matrix()

data_x = nd_data[:, :-1]
data_y = nd_data[:, -1]

# train/test 90/10% split
train_X, test_X, train_y, test_y =\
    train_test_split(data_x, data_y, test_size=0.1, random_state=0)

# print "data_train_X", data_train_X.shape
# print "data_train_y", data_train_y.shape

# First using basic Naive Bayes models to determine the baseline
print
print "Training MultinomialNB classifier.\n Please wait..."
clf = MultinomialNB()
train.classification(clf, train_X, train_y, test_X, test_y, "MultinomialNB")

print
print "Training GaussianNB classifier.\n Please wait..."
clf = GaussianNB()
train.classification(clf, train_X, train_y, test_X, test_y, "GaussianNB")

print
print "Training BernoulliNB classifier.\n Please wait..."
clf = BernoulliNB()
train.classification(clf, train_X, train_y, test_X, test_y, "BernoulliNB")

# Basic Multi-layer Perceptron classifier

# hidden layer = (input features + classes) / 2 i.e. approx 150
print
print "Training MLPClassifier classifier.\n Please wait..."
clf = MLPClassifier(hidden_layer_sizes=(150, )
                    # , activation='logistic'
                    # , solver='sgd'
                    # , learning_rate='adaptive'
                    )
train.classification(clf, train_X, train_y, test_X, test_y, "MLPClassifier")

# ## 3. Show with some X-validation the power of your model and
#  comment the results.                                                   ## #

print "Cross-validation on MultinomialNB classifier.\n Please wait..."
clf = MultinomialNB()
scores = cross_val_score(clf, data_x, data_y, cv=10)

# print scores
print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)
