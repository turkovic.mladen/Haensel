#! python2
import numpy as np


def negative_2_zero(data, column):
    data.loc[data.iloc[:, column] < 0, column] = 0
    return data


def log_transform(data, column):
    data[column] = np.log(data[column]+1)
    return data
