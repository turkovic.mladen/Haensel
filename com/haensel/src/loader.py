#! python2
import pandas as pd
from pathlib import Path


def get_csv(file_name, force=False):
    """
    :type file_name: str
    :type force: bool
    """

    my_file = Path(file_name+".pkl")
    if (not force) & my_file.is_file():
        # if pkl file exists, read file and return
        return pd.read_pickle(file_name+".pkl")

    # else: # i.e. force == True

    df = pd.read_csv(file_name, header=None, index_col=None, parse_dates=False)

    # save to pickle fast file
    df.to_pickle(file_name+".pkl")
    return df
