# Report

## Steps undertaken:

### 1. Make some initial data analysis.
_Try to find some structures in the data and how attributes are
 possibly connected._

#### Code
The data analysis is partially automatised by the analyse.py code which
 includes methods:

   * me <br>
        providing an overview of data minimum and maximum values for
        columns as also data types
   * pearson <br>
        generating the Pearson correlation coefficient for all columns
   * overview <br>
        printing the pandas.DataFrame.describe outcomes, mean, min and max,
        percentile values, standard deviation

These results are partially also printed to the Haensel/output folder for
further analysis.

Print the class distribution:

    Class Distribution:
    C    46882
    D     9279
    B     6602
    E     2507
    A      867

And an attempt at plot with plot.py

* only few columns are printed 1, 2 and 3

#### Results

Class distribution is heavily skewed and this can cause serious problems
for ML approach as the most frequent class (here "C") will be
over-represented in generated predictions.

Upon reviewing the outputs it is clear that only columns 23 and 36
contain some negative value data-points.

  * column 23 <br>
  as negative value contains only -1 and no zeroes. Presumably encoding
  a _"zero state"_ as "-1" (this interpretation should be confirmed)<br>
  <b>Therefore, all negative values were converted to zero</b>

  * column 36 <br>
  contains only 7 instances of negative values, which vary very widely.
  Without additional information and given the really low number of
  these occurrences they effectively prevent any meaningful use as
  predictors<br>
  <b>Therefore, all negative values (7 of them) were converted to zero</b>

Now I analysed the value distribution for single columns. Most columns
 (features) fall into the data range 0-1 while only few of them break
 this consistency.

  * columns 3, 36, 43, 64, 294 <br>
  contain values significantly exceeding 1, up-to tens of thousands.
  Since some ML algorithms are quite susceptible to such disproportionate
  values <br>
  <b>I applied a log transformation to reduce these differences</b>

Some columns, features contain absolutely no variation:

  * columns 59, 179 and 268 to 276 <br>
  contain only a single value without variation, standard deviation = 0<br>
  <b>Removed the columns before training the ML models</b>

Last thing to be done is to remove some of the columns with very high
correlation values

 * 4 & 44 (Pearson: 0.891462) or<br>
 * 45 & 6 (P: 0.763352) or <br>
 * 64 & 294 (P: 0.748347)<br>
 <b>Added to Next steps</b>

### 2. Fit some ML model(s).
_Under the models should definitely be some Neural Network and please
 explain briefly your choices._

Fitting only a handful of Naive Bayes models in order to determine a
meaningful Baseline result

 * Naive Bayes models Multinomial / Gaussian / Bernoulli

A basic Neural Network

 * Multi-layer Perceptron MLPClassifier
    * Using default configuration with 150 hidden layer nods
    (approximately a mean between input and output layer nods)
    * further, to simulate neuronal activation I would use:<br>
      , activation='logistic'<br>
      , solver='sgd'<br>
      , learning_rate='adaptive'<br>

#### Code
Using classification method from trainer.py which fits the model and
then outputs the accuracy, confusion matrix and more detailed
calculation of Precision, Recall, F1 and Support values

### 3. Show with some X-validation the power of your model
 _and comment the results._

All models are first validated by simple train/test split using 90/10%
ratio. Only the Multinomial Naive Bayes is evaluated through 10-fold
cross-validation since it is using a lot of time and Python's standard
functions do not allow to cross-validate multiple values at the same time.
Therefore developing an additional function (similar to trainer.py
classification) would be required.


#### MultinomialNB
This is the algorithm with highest accuracy

    Accuracy score:  0.668128212882

    Confusion matrix
         A    B    C    D    E
    [[   1   11   54    7    4]
     [   7   66  490   76   18]
     [  20  186 4222  234   41]
     [   6   63  737  118   13]
     [   5   23  167   33   12]]

    Classification report:
                 precision    recall  f1-score   support

              A       0.03      0.01      0.02        77
              B       0.19      0.10      0.13       657
              C       0.74      0.90      0.81      4703
              D       0.25      0.13      0.17       937
              E       0.14      0.05      0.07       240

    avg / total       0.59      0.67      0.62      6614

####  GaussianNB
This is the algorithm with highest precision

    Accuracy score:  0.223465376474

    Confusion matrix
         A    B    C    D    E
    [[  60    3    1    3   10]
     [ 444   17   57   55   84]
     [2486  151 1248  288  530]
     [ 585   15   89  100  148]
     [ 146    6   19   16   53]]

    Classification report:
                 precision    recall  f1-score   support

              A       0.02      0.78      0.03        77
              B       0.09      0.03      0.04       657
              C       0.88      0.27      0.41      4703
              D       0.22      0.11      0.14       937
              E       0.06      0.22      0.10       240

    avg / total       0.67      0.22      0.32      6614


####    BernoulliNB

     Accuracy score:  0.652706380405

    Confusion matrix
         A    B    C    D    E
    [[   1   14   50   10    2]
     [  18   82  448   88   21]
     [  35  222 4086  305   55]
     [  14   65  708  129   21]
     [   7   24  160   30   19]]

    Classification report:
                 precision    recall  f1-score   support

              A       0.01      0.01      0.01        77
              B       0.20      0.12      0.15       657
              C       0.75      0.87      0.80      4703
              D       0.23      0.14      0.17       937
              E       0.16      0.08      0.11       240

    avg / total       0.59      0.65      0.62      6614


#### MLPClassifier
Best balanced, all values are relatively high.
Highest precision for Class A (extremely rare and hard to predict)

    Accuracy score:  0.656486241306

    Confusion matrix
         A    B    C    D    E
    [[   5   16   39   12    5]
     [  15  114  411   88   29]
     [  25  211 4032  318  117]
     [  13   94  614  154   62]
     [   4   31  126   42   37]]

    Classification report:
                 precision    recall  f1-score   support

              A       0.08      0.06      0.07        77
              B       0.24      0.17      0.20       657
              C       0.77      0.86      0.81      4703
              D       0.25      0.16      0.20       937
              E       0.15      0.15      0.15       240

    avg / total       0.62      0.66      0.63      6614

### Conclusion

1. A lot more time should be invested in discovering and understanding data
  sources before undertaking any further data transformations.
2. Additional ML algorithms must be tested (SVMs, Trees, NNs,
ensemble methods... ) before trying deep-learning
3. Some time must be invested in understanding the business case for
doing this, and the actual goal of the process as whole.<br>
Is accuracy or precision more important? Are all classes are equally
important/valuable? Should classes be balanced for ML purposes?


### Next Steps:

* column 23<br>
 validate the decision to convert "-1" -> "0"

* column 36<br>
 validate the decision to convert all negative values -> "0"

* remove some of the columns with very high correlation values
 * 4 & 44 (Pearson: 0.891462) or
 * 45 & 6 (P: 0.763352) or
 * 64 & 294 (P: 0.748347)

* Enhance plot functionality to further explore the data

* New method generating detailed report over cross-validated models

* Gain a more detailed insight in the whole process e.g. by speaking to
 professionals currently assigning classes to values, in case currently
 this is the case. Speak with experts on how the data is collected;
 business people on what is the ultimate goal and purpose, or to sales
 department to establish the marketability of the product
