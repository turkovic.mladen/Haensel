
# Console output

    C:\Users\Mladen\MyPrograms\Python-2-7-12\python.exe C:/Users/Mladen/PycharmProjects/Haensel/com/haensel/src/main.py
    data_max > 1
    7
    data_min < 0
    2
     > Type overview>
    {dtype('int64'): Int64Index([  0,   1,   2,   4,   5,   6,   7,   8,   9,  10,
                ...
                284, 285, 286, 287, 288, 289, 290, 291, 292, 293],
               dtype='int64', length=291), dtype('float64'): Int64Index([3, 43, 64, 294], dtype='int64')}
     >>  Pearson correlation coefficients:
    Class Distribution:
    C    46882
    D     9279
    B     6602
    E     2507
    A      867
    Name: 295, dtype: int64
    plotting column_1....
    plotting column_2....
    plotting column_3....

    Training MultinomialNB classifier.
     Please wait...
    Saving classification model as ../models/MultinomialNB.pkl ...
     Accuracy score:  0.668128212882

    Confusion matrix
        A	B	C	D	E
    [[   1   11   54    7    4]
     [   7   66  490   76   18]
     [  20  186 4222  234   41]
     [   6   63  737  118   13]
     [   5   23  167   33   12]]
    Classification report:
                 precision    recall  f1-score   support

              A       0.03      0.01      0.02        77
              B       0.19      0.10      0.13       657
              C       0.74      0.90      0.81      4703
              D       0.25      0.13      0.17       937
              E       0.14      0.05      0.07       240

    avg / total       0.59      0.67      0.62      6614


    Training GaussianNB classifier.
     Please wait...
    Saving classification model as ../models/GaussianNB.pkl ...
     Accuracy score:  0.223465376474

    Confusion matrix
        A	B	C	D	E
    [[  60    3    1    3   10]
     [ 444   17   57   55   84]
     [2486  151 1248  288  530]
     [ 585   15   89  100  148]
     [ 146    6   19   16   53]]
    Classification report:
                 precision    recall  f1-score   support

              A       0.02      0.78      0.03        77
              B       0.09      0.03      0.04       657
              C       0.88      0.27      0.41      4703
              D       0.22      0.11      0.14       937
              E       0.06      0.22      0.10       240

    avg / total       0.67      0.22      0.32      6614


    Training BernoulliNB classifier.
     Please wait...
    Saving classification model as ../models/BernoulliNB.pkl ...
     Accuracy score:  0.652706380405

    Confusion matrix
        A	B	C	D	E
    [[   1   14   50   10    2]
     [  18   82  448   88   21]
     [  35  222 4086  305   55]
     [  14   65  708  129   21]
     [   7   24  160   30   19]]
    Classification report:
                 precision    recall  f1-score   support

              A       0.01      0.01      0.01        77
              B       0.20      0.12      0.15       657
              C       0.75      0.87      0.80      4703
              D       0.23      0.14      0.17       937
              E       0.16      0.08      0.11       240

    avg / total       0.59      0.65      0.62      6614


    Training MLPClassifier classifier.
     Please wait...
    Saving classification model as ../models/MLPClassifier.pkl ...
     Accuracy score:  0.656486241306

    Confusion matrix
        A	B	C	D	E
    [[   5   16   39   12    5]
     [  15  114  411   88   29]
     [  25  211 4032  318  117]
     [  13   94  614  154   62]
     [   4   31  126   42   37]]
    Classification report:
                 precision    recall  f1-score   support

              A       0.08      0.06      0.07        77
              B       0.24      0.17      0.20       657
              C       0.77      0.86      0.81      4703
              D       0.25      0.16      0.20       937
              E       0.15      0.15      0.15       240

    avg / total       0.62      0.66      0.63      6614

    Cross-validation on MultinomialNB classifier.
     Please wait...
    Accuracy: 0.66 (+/- 0.01)

